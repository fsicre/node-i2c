const usleep = require('sleep').usleep;

// ============================================================================
// Adafruit PCA9685 16-Channel PWM Servo Driver
// ============================================================================
module.exports = PCA9685

function PCA9685(i2c, {debug, address}) {
  // Registers/etc.
  const MODE1 = 0x00;
  const MODE2 = 0x01;
  const SUBADR1 = 0x02;
  const SUBADR2 = 0x03;
  const SUBADR3 = 0x04;
  const PRESCALE = 0xFE;
  const LED0_ON_L = 0x06;
  const LED0_ON_H = 0x07;
  const LED0_OFF_L = 0x08;
  const LED0_OFF_H = 0x09;
  const ALL_LED_ON_L = 0xFA;
  const ALL_LED_ON_H = 0xFB;
  const ALL_LED_OFF_L = 0xFC;
  const ALL_LED_OFF_H = 0xFD;

  // Bits:
  const RESTART = 0x80;
  const SLEEP = 0x10;
  const ALLCALL = 0x01;
  const INVRT = 0x10;
  const OUTDRV = 0x04;

  address = address||0x40;

  this.init = function() {
    if (debug) {
      console.log(`PCA9685 is at device ${i2c.device}, address:${address}`);
      console.log(`Reseting mode1: ${MODE1}`);
    }
    return this.setAllPWM(0, 0)
      // go to sleep
      .then(function() { return i2c.writeByte(address, MODE2, OUTDRV); })
      .then(function() { return i2c.writeByte(address, MODE1, ALLCALL); })
      // wait for oscillator 50ms
      .then(function() { usleep(5000); })
      // wake up (reset sleep)
      .then(function() { return i2c.readByte(address, MODE1); })
      .then(function(mode1) { return i2c.writeByte(address, MODE1, mode1 & ~SLEEP); })
      // wait for oscillator 50ms
      .then(function() { usleep(5000); })
      // done
      .then(function() { debug ? console.log('PCA9685 init done') : ''; })
      .catch(function(e) { console.error('PCA9685 error in init', e); });
  };

  let chipFrequency = 25000000.0; // 25 MHz
  let chipResolution = 4096.0; // 12-bits

  // Sets the PWM frequency
  this.setPWMFreq = function(freq, minus, maxus) {
    let estimated = chipFrequency / (chipResolution * freq) -1;
    let prescale = Math.floor(estimated + 0.5);

    this.frequency = chipFrequency / (prescale * chipResolution); // Hz
    this.periodus = (1000000 * prescale * chipResolution) / chipFrequency; // us
    this.minus = minus;
    this.maxus = maxus;
    this.minTicks = (0.0+minus) * chipResolution / this.periodus;
    this.maxTicks = (0.0+maxus) * chipResolution / this.periodus;

    if (debug) {
      console.log(`PCA9685 Setting PWM frequency to ${freq} Hz`);
      console.log(`PCA9685 Estimated pre-scale: ${estimated}`);
      console.log(`PCA9685 Final pre-scale: ${prescale}`);
      console.log(`PCA9685 Final frequency: ${this.frequency} Hz`);
      console.log(`PCA9685 Final period: ${this.periodus / 1000} ms`);
      console.log(`PCA9685   0% ~ ${Math.round(this.minTicks)} vs ${chipResolution}`);
      console.log(`PCA9685 100% ~ ${Math.round(this.maxTicks)} vs ${chipResolution}`);
    }

    let oldmode;
    return i2c.readByte(address, MODE1)
      .then(function (mode1) {
          oldmode = mode1;
          let newmode = (oldmode & 0x7F) | 0x10; // sleep
          if (debug) {
            console.log(`PCA9685 prescale ${prescale}, oldmode: 0x${oldmode.toString(16)} newMode: 0x${newmode.toString(16)}`);
          }
          // go to sleep
          return i2c.writeByte(address, MODE1, newmode);
        })
      .then(function() { return i2c.writeByte(address, PRESCALE, prescale); })
      .then(function() { return i2c.writeByte(address, MODE1, oldmode) })
      .then(function() { usleep(5000); })
      .then(function() { return i2c.writeByte(address, MODE1, oldmode | 0x80); });
  };

  // Sets a single PWM channel
  this.setPWMPercent = function(channel, percent) {
    percent = 0.0+percent;
    let on = 0;
    let off = Math.floor(this.minTicks + (0.0 + percent) * (this.maxTicks - this.minTicks) / 100);
    if (debug) {
      console.log(`PCA9685 Setting PWM channel, channel: ${channel}, percent : ${percent}`)
    }
    return this.setPWM(channel, on, off);
  };

  // Sets ALL PWM channels
  this.setAllPWMPercent = function(percent) {
    let on = 0;
    let off = Math.floor(this.minTicks + (0.0 + percent) * (this.maxTicks - this.minTicks) / 100);
    if (debug) {
      console.log(`PCA9685 Setting ALL PWM channels, percent : ${percent}`)
    }
    return this.setAllPWM(on, off);
  };

  // Sets a single PWM channel
  this.setPWM = function(channel, on, off) {
    if (debug) {
      console.log(`PCA9685 Setting PWM channel, channel: ${channel}, on : ${on} off ${off}`)
    }
    return                      i2c.writeByte(address,  LED0_ON_L + 4 * channel, on & 0xFF)
      .then(function() { return i2c.writeByte(address,  LED0_ON_H + 4 * channel, on >> 8); })
      .then(function() { return i2c.writeByte(address, LED0_OFF_L + 4 * channel, off & 0xFF); })
      .then(function() { return i2c.writeByte(address, LED0_OFF_H + 4 * channel, off >> 8); });
  };

  // Sets ALL PWM channels
  this.setAllPWM = function (on, off) {
    if (debug) {
      console.log(`PCA9685 Setting ALL PWM channels, on : ${on} off ${off}`)
    }
    return                      i2c.writeByte(address, ALL_LED_ON_L, on & 0xFF)
      .then(function() { return i2c.writeByte(address, ALL_LED_ON_H, on >> 8); })
      .then(function() { return i2c.writeByte(address, ALL_LED_OFF_L, off & 0xFF); })
      .then(function() { return i2c.writeByte(address, ALL_LED_OFF_H, off >> 8); });
  };

  // Stops ALL PWM channels
  this.stop = function() {
    if (debug) {
      console.log(`PCA9685 Stopping`);
    }
    return i2c.writeByte(address, ALL_LED_OFF_H, 0x10);
  };

}

