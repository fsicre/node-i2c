const PCA9685 = require('./PCA9685');

module.exports = HXT900;

function HXT900(pca9685, channel, {debug}) {
  this.pca9685 = pca9685;
  this.channel = channel;
}

HXT900.prototype.percent = function(percent) {
  return this.pca9685.setPWMPercent(this.channel, percent);
};
