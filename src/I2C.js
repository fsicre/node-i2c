const Bus = require('i2c-bus')

function I2C (device, {debug}) {
  this.device = '/dev/i2c-'+(device||1);

  const _self = this;
  const bus = Bus.openSync(device);

  this.readByte = function (address, cmd){
    if(debug){
      console.log(`I2C readByte address 0x${address.toString(16)} cmd 0x${cmd.toString(16)}`);
    }
    return new Promise(function (resolve, reject) {
        bus.readByte(address, cmd, function(error, data) {
            if (error) reject(error); else {
              console.log(`I2C readByte address 0x${address.toString(16)} cmd 0x${cmd.toString(16)} value 0x${data.toString(16)}`);
              resolve(data);
            }
          });
    });
  }

  this.writeByte = function (address, cmd, buf){
    if(debug){
      console.log(`I2C writeByte address 0x${address.toString(16)} cmd 0x${cmd.toString(16)} value 0x${buf.toString(16)}`)
    }
    return new Promise(function (resolve, reject) {
      bus.writeByte(address, cmd, buf, function(error, data) {
        if (error) reject(error); else resolve(data); 
      });
    });
  }
}

module.exports = I2C
