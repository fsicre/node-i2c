const sleep = require('sleep').sleep;
const usleep = require('sleep').usleep;
const I2C = require('./src/I2C');
const PCA9685 = require('./src/PCA9685');
const HXT900 = require('./src/HXT900');

const wire = new I2C(1, {debug: true});
const pwm = new PCA9685(wire, {address: 0x40, debug: true});
const hxt0 = new HXT900(pwm, 0, {debug: true});
const hxt1 = new HXT900(pwm, 1, {debug: true});

const min = 1;
const max = 2;
function between() {
  return Math.round((min + max * Math.random())) * 1000 * 1000; // us
}

pwm.init()
  .then(function() {
    // PWM periode : pour le HXT900 20ms => ~50Hz, pulse min / max duration in us (92 506)
    return pwm.setPWMFreq(41, 450, 2450); // semble bien mieux marcher que 50 Hz
  })

  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(65); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(80); })
  .then(function() { now(); usleep(between()) }).then(function() { hxt0.percent(100); return hxt1.percent(50); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(65); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(80); })
  .then(function() { now(); usleep(between()) }).then(function() { hxt0.percent(95); return hxt1.percent(50); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(65); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(80); })
  .then(function() { now(); usleep(between()) }).then(function() { return pwm.setAllPWMPercent(65); })

  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(0); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(25); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(50); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(75); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(100); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(50); })
  //.then(function() { now(); usleep(between) }).then(function() { return hxt0.percent(0); })

  .then(function() { now(); sleep(1); }).then(function() { return pwm.stop(); })
  .then(function() { now(); });

let ts=(new Date()).getTime();
function now() {
  console.log("elapsed",(new Date()).getTime()-ts, "ms");
}

